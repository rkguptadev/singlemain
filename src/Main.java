public class Main {
    public static void main(String...arg)
    {
        int[] numbers = new int[]{2,2,4,8};
        for(int i=1;i<5;i++)
            for(int j=1;j<5;j++)
                for(int k=1;k<5;k++)
                {
                    double result = evaluateExpression(evaluateExpression(evaluateExpression(numbers[0],numbers[1],i),numbers[2],j),numbers[3],k);
                    String expression = resolveExressionIntoString(i, j, k, numbers);
                    System.out.println(expression+" = "+result);
                }

    }
    public static double evaluateExpression(double first, double second, int operationCode)
    {
        switch (operationCode)
        {
            case 1: return first + second;
            case 2: return first - second;
            case 3: return first / second;
            case 4: return first * second;
            default: return 0;
        }
    }
    public static String resolveExressionIntoString(int i, int j, int k, int[] numbers)
    {
        return numbers[0]+resolveOperatorCode(i)+numbers[1]+resolveOperatorCode(j)+numbers[2]+resolveOperatorCode(k)+numbers[3];
    }
    public static String resolveOperatorCode(int operatorCode)
    {
        switch (operatorCode) {
            case 1:
                return "+";
            case 2:
                return "-";
            case 3:
                return "/";
            case 4:
                return "x";
            default:
                return "";
        }
    }
}
